const { DataStore } = require('notarealdb');

const store = new DataStore('./graphql/data');

module.exports = {
   posts:store.collection('posts')
};