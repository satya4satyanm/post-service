const db = require('./data/db')
const postsService = require('../posts/posts.service');


const Mutation = {
   createPost:(root,args,context,info) => {


         // create mongo db entry for posts as well
         postsService.createPost({
            name:args.name,
            description:args.description,
            type:args.type,
            toppings:args.toppings,
            price:args.price
      });


      // This creates notarealdb entry
         return db.posts.create({
            name:args.name,
            description:args.description,
            type:args.type,
            toppings:args.toppings,
            price:args.price
      })
   }
}
const Query = {
   greeting:() => "hello"
}

module.exports = {Query,Mutation}