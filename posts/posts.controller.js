﻿const express = require('express');
const router = express.Router();
const postsService = require('./posts.service');

// routes
router.get('/getAllPosts', getAll);

module.exports = router;

function getAll(req, res, next) {
    postsService.getAll()
        .then(posts => {
            if(typeof posts === "string")
                posts = JSON.parse(posts);

            res.json(posts)
        })
        .catch(err => next(err));
}