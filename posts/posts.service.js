﻿const db = require('_helpers/db');
const Posts = db.Posts;


// DO NOT remove below lines that is for redis
// const redisClient = require('redis').createClient;
// const redis = redisClient(6379, 'localhost');

// redis.on("connect", () => {
//     console.log('connected to Redis');
// });


module.exports = {
    getAll,
    createPost
};

async function getAll() {

// DO NOT remove below lines that is for redis

    // return new Promise((resolve, reject) => {
    //     redis.get("postList",(err, reply) => {
    //         if(err) {
    //             console.log(err);
    //         } else if(reply) {
    //             console.log("post list served from redis.")
    //             resolve(reply);
    //         } else {
    //             Post.find({}, (err, posts) => {
    //                 if(err) {
    //                     return reject(err);
    //                 }
    //                 if(posts.length > 0) {
    //                     // set in redis
    //                     redis.set("postList", JSON.stringify(posts));
    //                 }
    //                 console.log("post list served from mongo.")
    //                 resolve(posts);
    //             });
    //         }
    //     });
    // });



    return await Posts.find({});
}

async function createPost(args) {
    const post = new Posts(args);
    // save post
    await post.save();
}

